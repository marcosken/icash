import styled from "styled-components";

export const DashboardPageWrapper = styled.div`
  margin: auto;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`;

export const SubHeader = styled.div`
  margin: 10px auto;
  width: 65%;
  display: flex;
  align-items: center;
  justify-content: space-between;

  .imgLogo {
    width: 30%;

    img {
      width: 100%;
    }

    @media only screen and (min-width: 900px) {
      width: 20%;
    }
  }

  .searchIcon {
    width: 50px;
    height: 50px;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 50%;
    background-color: var(--grayHome);
    cursor: pointer;
  }
`;

export const Stores = styled.div`
  width: 80%;
  margin: auto;
  display: flex;
  flex-wrap: wrap;
  gap: 20px;
  justify-content: center;
  align-items: center;

  @media only screen and (min-width: 576px) {
    width: 90%;

    .carousel {
      display: none;
    }
  }
`;

export const Grid = styled.div`
  display: none;
  padding-top: 24px;
  @media only screen and (min-width: 576px) {
    display: flex;
    gap: 50px;
    flex-wrap: wrap;
    justify-content: center;
  }
`;

export const Footer = styled.div`
  background-color: var(--grayHome);
  width: 100vw;
  height: 12vh;
  display: flex;
  color: var(--orangeCore);
  display: flex;
  font-weight: 600;
  margin-top: 20px;
  align-items: center;
  justify-content: space-between;

  .footerWrapper {
    margin: auto;
    width: 85%;
    display: flex;
    align-items: center;
    justify-content: space-between;

    .profilePic {
      width: 15%;

      img {
        width: 100%;
        border-radius: 50%;
        cursor: pointer;
      }

      @media only screen and (min-width: 600px) {
        width: 8%;
      }

      @media only screen and (min-width: 1000px) {
        width: 6%;
      }
    }
  }

  .infoWallet {
    display: flex;
    justify-content: space-between;
    align-items: center;
    gap: 10px;
    width: 40%;

    @media only screen and (min-width: 768px) {
      width: 40%;
    }
  }

  @media only screen and (min-width: 1000px) {
    visibility: hidden;
  }
`;
