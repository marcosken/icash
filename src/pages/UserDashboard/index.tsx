import {
  DashboardPageWrapper,
  SubHeader,
  Stores,
  Grid,
  Footer,
} from "./styles";
import SearchIcon from "../../assets/search.svg";
import FormLogo from "../../assets/LogoForm.svg";
import { useState } from "react";
import WalletIcon from "../../assets/wallet.svg";
import { useHistory } from "react-router-dom";
import SearchStoreModal from "../../components/searchStoresModal";
import { useStoreRegister } from "../../providers/store-register";
import Carousel from "react-material-ui-carousel";
import { useUpdate } from "../../providers/UserProvider";
import formatValue from "../../utils/formatValue";
import Avatar from "../../assets/avatar.png";
import loading from "../../assets/loading.svg";
import StoreCard from "../../components/StoreCard";
import HeaderComponent from "../../components/HeaderComponent";
import ContainerAnimation from "../../components/ContainerAnimation";

const UserDashboard = () => {
  const { stores, isLoading } = useStoreRegister();
  const history = useHistory();
  const [modalIsOpen, setIsOpen] = useState<boolean>(false);
  const openModal = () => setIsOpen(true);

  const { user } = useUpdate();
  const { cashback } = user;
  const userCashback = formatValue(cashback);

  return (
    <ContainerAnimation>
      <DashboardPageWrapper>
        <HeaderComponent isDashboard />
        <SubHeader>
          <figure className="imgLogo">
            <img src={FormLogo} alt="icash-login-form" />
          </figure>
          <figure onClick={openModal} className="searchIcon">
            <img src={SearchIcon} alt="find-stores" />
          </figure>
        </SubHeader>
        <SearchStoreModal
          modalIsOpen={modalIsOpen}
          setIsOpen={setIsOpen}
          openModal={openModal}
        />
        {isLoading ? (
          <img src={loading} alt="loading" />
        ) : (
          <Stores>
            <Carousel
              fullHeightHover={false}
              navButtonsAlwaysInvisible={true}
              indicators={false}
              interval={4000}
              animation="slide"
              className="carousel"
            >
              {stores.map((store, index) => (
                <StoreCard key={index} store={store} />
              ))}
            </Carousel>
            <Grid>
              {stores.map((store, index) => (
                <StoreCard key={index} store={store} />
              ))}
            </Grid>
          </Stores>
        )}
        <Footer>
          <div className="footerWrapper">
            <div className="infoWallet">
              <img src={WalletIcon} alt="go-to-user-transactions" />
              <span>{userCashback}</span>
            </div>
            <figure className="profilePic">
              <img
                onClick={() => {
                  history.push("/user-profile");
                }}
                src={Avatar}
                alt="go-to-user-profile"
              />
            </figure>
          </div>
        </Footer>
      </DashboardPageWrapper>
    </ContainerAnimation>
  );
};

export default UserDashboard;
