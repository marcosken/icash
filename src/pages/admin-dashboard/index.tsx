import { Link } from "react-router-dom";
import { Container, Containerfull, Header } from "../admin-profile/styles";
import arrowLeft from "../../assets/arrowLeft.svg";
import { Stores, ContainerStore, ContainerBtn } from "./styles";
import { useStoreRegister } from "../../providers/store-register";
import ModalUpdateStore from "../../components/ModalUpdateStore";
import ContainerAnimation from "../../components/ContainerAnimation";

const AdminDasboard = () => {
  const { setShowModalStore, showModalStore, stores, setIdStore } =
    useStoreRegister();

  return (
    <ContainerAnimation>
      <Containerfull>
        {showModalStore && <ModalUpdateStore />}
        <Container>
          <Link to="/admin-profile">
            <img src={arrowLeft} alt="voltar" />
          </Link>
        </Container>
        <h2>Lojas Cadastradas</h2>
        <Header />
        <Stores>
          {stores.map((store, index) => (
            <ContainerStore key={store.id}>
              <figure>
                <img
                  key={index}
                  className="image"
                  src={store.store_img}
                  alt={store.name}
                />
              </figure>
              <p>
                <span>{store.name}</span>
                <div>
                  {store.city} - {store.cashback}%
                </div>
              </p>
              <p>{store.category}</p>
              <ContainerBtn>
                <button
                  onClick={() => {
                    setShowModalStore(true);
                    setIdStore(store.id);
                  }}
                >
                  Atualizar Dados
                </button>
              </ContainerBtn>
            </ContainerStore>
          ))}
        </Stores>
      </Containerfull>
    </ContainerAnimation>
  );
};

export default AdminDasboard;
