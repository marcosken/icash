import { LoginPageContainer, FormWrapper } from "./styles";
import RegisterForm from "../../components/RegisterForm";
import ContainerAnimation from "../../components/ContainerAnimation";

const SignUp = () => {
  return (
    <ContainerAnimation>
      <LoginPageContainer>
        <FormWrapper>
          <RegisterForm />
        </FormWrapper>
      </LoginPageContainer>
    </ContainerAnimation>
  );
};

export default SignUp;
