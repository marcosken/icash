import { PageWrapper } from "./styles";
import { Link } from "react-router-dom";
import ContainerAnimation from "../../components/ContainerAnimation";

const iCashTeam = [
  {
    name: "Ariel Bernardes",
    role: "Teach Lead",
    img: "https://media-exp1.licdn.com/dms/image/C4E03AQG1dX6UlagLaA/profile-displayphoto-shrink_200_200/0/1617215000737?e=1655337600&v=beta&t=LSxTlAuNlYxU8skQZrgKJAvVCWd5jNFbzZyE_MYIE5Y",
  },
  {
    name: "Marcos Kuribayashi ",
    role: "Product Owner",
    img: "https://gitlab.com/uploads/-/system/user/avatar/8603970/avatar.png?width=400",
  },
  {
    name: "Jorge William",
    role: "Quality Assurance",
    img: "https://gitlab.com/uploads/-/system/user/avatar/8603440/avatar.png?width=400",
  },
  {
    name: "Pierre Kalil",
    role: "Quality Assurance",
    img: "https://gitlab.com/uploads/-/system/user/avatar/8603451/avatar.png?width=400",
  },
  {
    name: "Luiz Federico",
    role: "Scrum Master",
    img: "https://gitlab.com/uploads/-/system/user/avatar/8604666/avatar.png?width=400",
  },
];

const Icash = () => {
  return (
    <ContainerAnimation>
      <PageWrapper>
        <div>
          <h1>Quem somos</h1>
          <p>
            iCash nasce para ajudar os comerciantes a alavancarem seus negócios.
            Somos um marketplace, onde lojistas e clientes se encontram para
            vender e comprar. As lojas oferecem ao consumidor parte do dinheiro
            gasto na compra em forma de cashback. O cliente, por sua vez, vai
            acumulando o valor de cada compra, podendo utilizá-lo em toda a rede
            iCash.
          </p>
        </div>

        <h2>Nosso time</h2>
        <ul>
          {iCashTeam.map((dev, index) => (
            <li key={index}>
              <img src={dev.img} alt={dev.name} />
              <span>{dev.name}</span>
              <p>{dev.role}</p>
            </li>
          ))}
        </ul>
        <Link to="/user-profile">Voltar</Link>
      </PageWrapper>
    </ContainerAnimation>
  );
};

export default Icash;
