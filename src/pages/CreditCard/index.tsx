import AddCreditCardModal from "../../components/AddCreditCardModal";
import CardCredit from "../../components/CardCredit";
import { useCreditCards } from "../../providers/CreditCards";
import {
  CardsList,
  Container,
  Header,
  Subtitle,
  Title,
  SubHeader,
} from "./styles";
import loading from "../../assets/loading.svg";
import HeaderComponent from "../../components/HeaderComponent";
import BackButton from "../../components/BackButton";
import ContainerAnimation from "../../components/ContainerAnimation";

const CreditCard = () => {
  const { creditCards, isLoading } = useCreditCards();

  return (
    <ContainerAnimation>
      <Container>
        <Header />
        <BackButton />
        <HeaderComponent />
        <SubHeader>
          <Title>Cartão de Crédito</Title>
          <AddCreditCardModal />
        </SubHeader>
        {isLoading ? (
          <img src={loading} alt="loading" />
        ) : (
          <CardsList>
            {creditCards[0] !== undefined ? (
              creditCards.map((creditCard) => (
                <li key={creditCard.id}>
                  <CardCredit creditCard={creditCard} />
                </li>
              ))
            ) : (
              <Subtitle>
                Você ainda não tem cartões de crédito cadastrados.
              </Subtitle>
            )}
          </CardsList>
        )}
      </Container>
    </ContainerAnimation>
  );
};

export default CreditCard;
