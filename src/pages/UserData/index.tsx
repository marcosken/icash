import { ContainerUser, HeaderUser, AvatarUser, DataUser } from "./styles";
import UserUpdateModal from "../../components/userUpdateModal";
import { useUpdate } from "../../providers/UserProvider";
import Icon from "../../assets/avatar.png";
import BackButton from "../../components/BackButton";
import ContainerAnimation from "../../components/ContainerAnimation";

const UserData = () => {
  const { user } = useUpdate();
  const { name, email, cpf } = user;

  return (
    <ContainerAnimation>
      <ContainerUser>
        <HeaderUser />
        <BackButton />

        <AvatarUser>
          <img src={Icon} alt="perfil" />
          <UserUpdateModal />
        </AvatarUser>

        <DataUser>
          <p>Nome: {name}</p>
          <p>E-mail: {email}</p>
          <p>CPF: {cpf}</p>
        </DataUser>
      </ContainerUser>
    </ContainerAnimation>
  );
};

export default UserData;
