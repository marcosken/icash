import { LoginPageContainer, FormWrapper } from "./styles";
import LoginForm from "../../components/LoginForm";
import ContainerAnimation from "../../components/ContainerAnimation";

const Login = () => {
  return (
    <ContainerAnimation>
      <LoginPageContainer>
        <FormWrapper>
          <LoginForm />
        </FormWrapper>
      </LoginPageContainer>
    </ContainerAnimation>
  );
};

export default Login;
