import { useHistory, Link } from "react-router-dom";
import {
  ContainerUser,
  HeaderUser,
  BtnProfileUser,
  AvatarUser,
  FooterUser,
  ContainerBtn,
  ContainerAvatar,
  ModalButtons,
  ModalContent,
} from "./styles";
import { useUpdate } from "../../providers/UserProvider";
import Modal from "react-modal";
import { useState } from "react";
import "./styles.css";
import Button from "../../components/Button";
import Icon from "../../assets/avatar.png";
import { useAuth } from "../../providers/Auth";
import toast from "react-hot-toast";
import BackButton from "../../components/BackButton";
import ContainerAnimation from "../../components/ContainerAnimation";
import FormLogo from "../../assets/LogoForm.svg";

const UserProfile = () => {
  const history = useHistory();
  const { deleteAccount } = useUpdate();
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const { setToken } = useAuth();

  const toggleModal = () => {
    setModalIsOpen(!modalIsOpen);
  };

  const sendTo = (path: string) => {
    history.push(path);
  };

  const handleLogout = () => {
    setToken("");
    localStorage.clear();
    toast.success("Até breve!");
  };

  return (
    <ContainerAnimation>
      <ContainerUser>
        <HeaderUser />
        <BackButton isUserProfilePage />
        <ContainerAvatar>
          <AvatarUser>
            <img src={Icon} alt="perfil" />
          </AvatarUser>
        </ContainerAvatar>
        <ContainerBtn>
          <Link to="/dashboard">
            <img src={FormLogo} alt="icash-login-form" />
          </Link>
          <BtnProfileUser onClick={() => sendTo("/user-data")}>
            Dados pessoais
          </BtnProfileUser>
          <BtnProfileUser onClick={() => sendTo("/credit-cards")}>
            Cartão de crédito
          </BtnProfileUser>
          <BtnProfileUser onClick={() => sendTo("/icash")}>
            Sobre ICash
          </BtnProfileUser>
        </ContainerBtn>
        <FooterUser>
          <span onClick={toggleModal}>Cancelar conta</span>
          <span onClick={handleLogout}>Sair da sessão</span>
        </FooterUser>
        <Modal
          isOpen={modalIsOpen}
          ariaHideApp={false}
          onRequestClose={toggleModal}
          className="DeleteModal"
        >
          <ModalContent>
            <p>Deseja mesmo cancelar a sua conta ICash?</p>
            <ModalButtons>
              <Button onClick={deleteAccount}>Sim</Button>
              <Button onClick={toggleModal}>Não</Button>
            </ModalButtons>
          </ModalContent>
        </Modal>
      </ContainerUser>
    </ContainerAnimation>
  );
};

export default UserProfile;
