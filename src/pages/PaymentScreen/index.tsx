import {
  Container,
  Payment,
  PaymentOptions,
  Logo,
  TextMobile,
  TextDesktop,
  Wallet,
  Content,
  ICashCard,
  ICashback,
  BackArrow,
} from "./styles";
import WalletIMG from "../../assets/walletPaymentScreen.svg";
import ICashIMG from "../../assets/iCashPayScreenLogo.svg";
import ICashCardIMG from "../../assets/iCashCard.svg";
import IMGLogo from "../../assets/LogoHeaderPayment.svg";
import BackArrowIMG from "../../assets/ArrowLeftPayment.svg";
import { useHistory } from "react-router-dom";
import HeaderComponent from "../../components/HeaderComponent";
import ContainerAnimation from "../../components/ContainerAnimation";

const PaymentScreen = () => {
  const history = useHistory();

  return (
    <ContainerAnimation>
      <Container>
        <HeaderComponent />
        <Payment>
          <Wallet src={WalletIMG} alt="Carteira" />
          <Logo>
            <img src={IMGLogo} alt="Logo" />
          </Logo>
          <TextDesktop>
            <h2>Escolha a forma de pagamento</h2>
          </TextDesktop>
          <TextMobile>
            <h3>Forma de pagamento</h3>
          </TextMobile>
        </Payment>
        <PaymentOptions>
          <Content onClick={() => history.push("/cardpayment")}>
            <ICashCard src={ICashCardIMG} alt="Cartão" />
          </Content>
          <Content onClick={() => history.push("/cashbackpayment")}>
            <ICashback src={ICashIMG} alt="Cashback" />
          </Content>
        </PaymentOptions>
        <BackArrow onClick={() => history.push("/dashboard")}>
          <img src={BackArrowIMG} alt="Voltar" />
        </BackArrow>
      </Container>
    </ContainerAnimation>
  );
};

export default PaymentScreen;
