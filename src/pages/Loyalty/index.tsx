import { PageContainer, PageHeader, PageBody } from "./styles";
import Button from "../../components/Button";
import LoyaltyImg from "../../assets/loyalty.png";
import WhiteLogo from "../../assets/logoWhite.png";
import { useHistory } from "react-router-dom";
import ContainerAnimation from "../../components/ContainerAnimation";

const Loyalty = () => {
  const history = useHistory();

  const winASurprise = () => {
    history.push("/signup");
  };

  return (
    <ContainerAnimation>
      <PageContainer>
        <PageHeader>
          <div className="inputWrapper">
            <img src={WhiteLogo} alt="icash-logo" />
            <h3>Aperte o botão, crie a sua conta e ganhe uma surpresa!</h3>
            <Button type="button" onClick={winASurprise}>
              Quero ganhar uma surpresa
            </Button>
          </div>
        </PageHeader>
        <PageBody>
          <img src={LoyaltyImg} alt="loyalty-img" />
        </PageBody>
      </PageContainer>
    </ContainerAnimation>
  );
};

export default Loyalty;
