export interface userSignUpData {
  name: string;
  email: string;
  cpf: string;
  password: string;
  passwordConfirm: string;
  isAdmin: boolean;
  cashback: number;
}
