import { Card } from "./styles";
import { useHistory } from "react-router-dom";

interface Stores {
  name: string;
  address: string;
  city: string;
  category: string;
  cashback: number;
  working_hours: string;
  telephone: string;
  store_img: string;
  open: string;
  onSale: string;
  id: number;
}

interface StoreCardProps {
  store: Stores;
}

const StoreCard = ({ store }: StoreCardProps) => {
  const history = useHistory();

  return (
    <Card>
      <p>
        <span>{store.cashback}%</span> cashback
      </p>
      <img
        className="image"
        src={store.store_img}
        alt={store.name}
        onClick={() => {
          history.push(`/store/${store.id}`);
        }}
      />
      <p>
        <span>{store.name}</span>
      </p>
      <p>{store.city}</p>
      <p>{store.category}</p>
    </Card>
  );
};

export default StoreCard;
