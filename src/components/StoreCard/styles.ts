import styled from "styled-components";

export const Card = styled.div`
  p {
    color: var(--grayStandOut);
    margin-bottom: 15px;
    font-weight: 600;
    text-align: center;

    span {
      color: var(--orangeCore);
      font-weight: 600;
    }
  }

  .image {
    display: flex;
    height: 20pc;
    width: 200px;
    border-radius: 30px;
    cursor: pointer;
    border: 5px solid var(--lightOrange);
    margin-bottom: 10px;

    @media only screen and (min-width: 1440px) {
      height: 30pc;
    }
  }
`;
