import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  padding: 1rem;

  img {
    width: 30px;
    height: 30px;
    cursor: pointer;
  }

  @media screen and (min-width: 1000px) {
    display: none;
  }
`;
