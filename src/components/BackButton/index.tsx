import arrowLeft from "./../../assets/arrowLeft.svg";
import { Container } from "./styles";
import { useHistory } from "react-router-dom";
import { useState } from "react";

interface BackButtonProps {
  isUserProfilePage?: boolean;
}

const BackButton = ({ isUserProfilePage = false }: BackButtonProps) => {
  const history = useHistory();
  const [isProfilePage] = useState(isUserProfilePage);

  const goBackPage = () => {
    if (isProfilePage === true) {
      history.push("/dashboard");
    } else {
      history.goBack();
    }
  };

  return (
    <Container>
      <img onClick={goBackPage} src={arrowLeft} alt="Voltar" />
    </Container>
  );
};

export default BackButton;
