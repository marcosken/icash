import { Header } from "./styles";
import { useUpdate } from "../../providers/UserProvider";
import WalletIcon from "../../assets/wallet.svg";
import formatValue from "../../utils/formatValue";
import Avatar from "../../assets/avatar.png";
import { useHistory } from "react-router-dom";

interface HeaderComponentProps {
  isDashboard?: boolean;
}

const HeaderComponent = ({ isDashboard = false }: HeaderComponentProps) => {
  const { user } = useUpdate();
  const userCashback = formatValue(user.cashback);
  const history = useHistory();

  return (
    <Header isDashboard={isDashboard}>
      <div className="headerWrapper">
        <div>
          Olá, <span>{user.name}</span>
        </div>
        <div className="walletWrapper">
          <div className="infoWallet">
            <img src={WalletIcon} alt="go-to-user-transactions" />
            <span>{userCashback}</span>
            <figure className="profilePic">
              <img
                onClick={() => {
                  history.push("/user-profile");
                }}
                src={Avatar}
                alt="go-to-user-profile"
              />
            </figure>
          </div>
        </div>
      </div>
    </Header>
  );
};

export default HeaderComponent;
