import styled from "styled-components";

interface HeaderProps {
  isDashboard: boolean;
}

export const Header = styled.div<HeaderProps>`
  background-color: var(--orangeCore);
  width: 100vw;
  color: var(--lightOrange);
  font-size: 1.5rem;
  display: ${(props) => (props.isDashboard ? "flex" : "none")};

  .headerWrapper {
    margin: auto;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 8px 0;
  }

  .walletWrapper {
    display: flex;
    justify-content: space-between;
    align-items: center;
    display: none;

    .infoWallet {
      display: flex;
      justify-content: flex-end;
      align-items: center;
      width: 100%;
      gap: 10px;

      .profilePic {
        width: 12%;

        img {
          width: 100%;
          border-radius: 50%;
          cursor: pointer;
          border: 2px solid var(--fontLightGray);
        }

        @media only screen and (min-width: 1440px) {
          width: 20%;
        }
      }
    }

    @media only screen and (min-width: 1000px) {
      display: inline-flex;
    }

    @media only screen and (min-width: 1200px) {
      width: 40%;
    }

    @media only screen and (min-width: 1440px) {
      width: 35%;
    }

    @media only screen and (min-width: 2000px) {
      width: 30%;
    }
  }

  span {
    color: var(--white);
  }

  @media only screen and (min-width: 1000px) {
    display: flex;
    .headerWrapper {
      width: 65%;
      justify-content: space-between;
    }
  }
`;
