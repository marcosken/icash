import axios from "axios";

const api = axios.create({
  baseURL: "https://icashapi.herokuapp.com/",
});

export default api;
