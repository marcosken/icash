<h1 align="center">
    <img src="./src/assets/LogoForm.svg">
</h1>

<p align="center">
 <a href="#-sobre-o-projeto">Sobre o Projeto</a> •
 <a href="#-funcionalidades">Funcionalidades</a> • 
 <a href="#%EF%B8%8F-instalação">Instalação</a> • 
 <a href="#-layout">Layout</a> •
 <a href="#-tecnologias">Tecnologias</a> •  
 <a href="#-equipe">Equipe</a>
</p>

## 🖥️ Sobre o Projeto

> Projeto criado dentro do curso da **Kenzie Academy Brasil** como atividade final do módulo _Quarter 2_

Este projeto, basicamente, é um marketplace onde as lojas cadastradas oferecem parte do dinheiro gasto nas compras em forma de cashback. Assim, elas fidelizam o cliente e atraem mais clientes, além de obterem um aumento nas vendas e na sua visibilidade.

Os clientes podem ver a lista de todos os estabelecimentos participantes, comparar, ver quais apresentam mais vantagens, realizar pagamentos, receber e acumular cashback, bem como utilizar esse valor acumulado em uma carteira digital para realizar pagamentos de compras em toda a rede de lojas cadastradas na plataforma.

Com a oportunidade de ter de volta parte do dinheiro investido em uma compra, os clientes tendem a retornar ao estabelecimento para desfrutar novamente do benefício. Além disso, a possibilidade de utilizar o próprio dinheiro recebido como benefício para comprar mais uma vez, serve como um forte elemento atrativo para os consumidores e uma ferramenta de fidelização das lojas presentes na rede iCash.

**Acessar demonstração**: [![Vercel](https://img.shields.io/badge/vercel-%23000000.svg?style=for-the-badge&logo=vercel&logoColor=white)](https://icash-project.vercel.app/)

## 💡 Funcionalidades

- [x] Cadastro de cliente
- [x] Login de cliente
- [x] Exibição de todas as lojas
- [x] Pesquisa de lojas
- [x] Exibição de informações de uma loja
- [x] Cadastro de cartão de crédito
- [x] Pagamento via cartão de crédito
- [x] Pagamento via carteira digital iCash
- [x] Editar dados pessoais
- [x] Deletar conta iCash
- [x] Login de admin
- [x] Cadastro de lojas
- [x] Editar informações de uma loja
- [x] Excluir uma loja
- [ ] Histórico de transações

## ⚙️ Instalação

- Faça o fork desse repositório;

- Abra o terminal e clone o repositório:

```Bash
$ git clone git@gitlab.com:<your_user>/icash.git
```

- Entre no diretório do projeto

```Bash
$ cd icash
```

- Instale as dependências

```Bash
$ yarn install
```

- Execute a aplicação

```Bash
$ yarn start
```

Pronto! A aplicação, agora, pode ser acessada através da rota http://localhost:3000/.

## 🎨 Layout

O layout da aplicação está disponível no Figma: [![Figma](https://img.shields.io/badge/figma-%23F24E1E.svg?style=for-the-badge&logo=figma&logoColor=white)](https://www.figma.com/files/team/1011730596372712104/iCash-Capstone?fuid=999008294488265665)

<p align="center">
  <img src="./src/assets/home.png" width="200px">

  <img src="./src/assets/login.png" width="200px">

  <img src="./src/assets/dashboard.png" width="200px">

  <img src="./src/assets/credit-card.png" width="200px">

  <img src="./src/assets/icash-pay.png" width="200px">
</p>

## ⚒ Tecnologias

Para o desenvolvimento desse projeto, as seguintes ferramentas foram utilizadas:

- [React.js](https://pt-br.reactjs.org/)
- [Axios](https://axios-http.com/ptbr/)
- [React Hook Form](https://react-hook-form.com/)
- [Styled Components](https://styled-components.com/)
- [React Icons](https://react-icons.github.io/react-icons/)
- [React Modal](https://www.npmjs.com/package/react-modal)
- [React Material UI Carousel](https://www.npmjs.com/package/react-material-ui-carousel)
- [TypeScript](https://www.typescriptlang.org/pt/)
- [JSON Server](https://www.npmjs.com/package/json-server)
- [JSON Server Auth](https://www.npmjs.com/package/json-server-auth)

## 👥 Equipe

- Ariel Bernardes [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/ariel-bernardes-soares-404381144/)
- Jorge William [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/jorgewilliannagakura/)
- Luiz Federico [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/luiz-rafael-verrone-federico-07ba0017/)
- Marcos Kuribayashi [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/marcos-kuribayashi/)
- Pierre Kalil [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/pierre-kalil/)
